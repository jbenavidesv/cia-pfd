<?php
  include "Conection.php";
  $link = Conection::conect();
  $result = $link->query(
    "call getAssistances("
      . "'" . $_POST["idUser"] . "', "
      . "'" . $_POST["idCourse"] . "'"
    . ");"
  );
  $data = "";
  while ($row = mysqli_fetch_array($result)) {
    $data .=
      "<li class='assistances'>" .
        "<a class='dropdown-button' href='#!' data-activates='assistance_" . $row['id'] . "'>";
    if ($row['type'] == "Entrada"){
      $data .= "<i class='material-icons'>call_received</i>" . $row['assistanceDate'] ." - " . $row['assistanceHour'];
    } else {
      $data .= "<i class='material-icons'>call_made</i>" . $row['assistanceDate'] ." - " . $row['assistanceHour'];
    }
    $data .=
        "</a>" .
      "</li>" .
      "<ul id='assistance_" . $row['id'] . "' class='dropdown-content assistances'>" .
        "<li><a href='#!' class='deleteAssistance' idCourse='" . $row['id'] . "'>Eliminar</a></li>" .
      "</ul>";
  }
  $link->close();
  echo $data;
?>
