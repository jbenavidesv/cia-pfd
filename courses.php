
<!DOCTYPE html>
<html>
<head>
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
  <link rel="stylesheet" href="css/style.css">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body>
  <?php include "models/Conection.php"; ?>
  <?php include "navbar.php"; ?>
  <div class="container">
    <div class="divider"></div>
    <div class="section">
      <h5>Cursos</h5>
      <table class="striped">
        <tr>
          <th>Nombre</th>
          <th>Inicio</th>
          <th>Fin</th>
          <th>Frecuencia</th>
          <th>Horario</th>
          <th>Editar</th>
          <th>Eliminar</th>
        </tr>
        <?php
        $link = Conection::conect();
        $result = $link->query("call getCourses();");
        while ($row = mysqli_fetch_array($result)) {
          ?>
          <tr>
            <td><?php echo $row['name'] ?></td>
            <td><?php echo $row['startDate'] ?></td>
            <td><?php echo $row['endDate'] ?></td>
            <td><?php echo $row['frecuency'] ?></td>
            <td><?php echo $row['hourStart'] ?> - <?php echo $row['hourEnd'] ?></td>
            <td><a href="/pfd/course.php?id=<?php echo $row['id'] ?>" class="edit"><i class="material-icons">mode_edit</i></a></td>
            <td><a href="#" class="delete" idCourse="<?php echo $row['id'] ?>"><i class="material-icons">delete</i></a></td>
          </tr>
          <?php
        }
        $link->close();
        ?>
      </table>
    </div>
  </div>

  <div class="fixed-action-btn">
    <a class="btn-floating btn-large red" href="#modal1">
      <i class="large material-icons">add</i>
    </a>
  </div>

  <div id="modal1" class="modal">
    <div class="modal-content">
      <h5>Nuevo Curso</h5>
      <div class="row">
        <form id="form" class="col s12">
          <input type="hidden" name="id" value="">
          <div class="row">
            <div class="input-field col s6">
              <input id="name" type="text" class="validate">
              <label for="name">Nombre*</label>
            </div>
            <div class="input-field col s3">
              <input id="startDate" type="date" class="datepicker">
              <label for="startDate">Fecha de inicio</label>
            </div>
            <div class="input-field col s3">
              <input id="endDate" type="date" class="datepicker">
              <label for="endDate">Fecha de fin</label>
            </div>
          </div>
          <div class="row">
              <input type="checkbox" class="filled-in" id="1Day" name="frecuency" value="1"/>
              <label for="1Day">Lunes</label>
              <input type="checkbox" class="filled-in" id="2Day" name="frecuency" value="2"/>
              <label for="2Day">Martes</label>
              <input type="checkbox" class="filled-in" id="3Day" name="frecuency" value="4"/>
              <label for="3Day">Miércoles</label>
              <input type="checkbox" class="filled-in" id="4Day" name="frecuency" value="8"/>
              <label for="4Day">Jueves</label>
              <input type="checkbox" class="filled-in" id="5Day" name="frecuency" value="16"/>
              <label for="5Day">Viernes</label>
              <input type="checkbox" class="filled-in" id="6Day" name="frecuency" value="32"/>
              <label for="6Day">Sábado</label>
          </div>
          <div class="row">
            <div class="input-field col s3">
              <input id="hourStart" type="time" class="timepicker">
              <label for="hourStart">Hora de inicio</label>
            </div>
            <div class="input-field col s3">
              <input id="hourEnd" type="time" class="timepicker">
              <label for="hourEnd">Hora de fin</label>
            </div>
						<div class="input-field col s6">
							<select id="idUser">
								<option value="" disabled selected>Elige un profesor</option>
									<?php
										$link = Conection::conect();
										$result = $link->query("call getProfessors();");
										while ($row = mysqli_fetch_array($result)) {
									?>
										<option value="<?php echo $row['id'] ?>"><?php echo $row['name'] ?></option>
									<?php }
										$link->close();
									?>
							</select>
							<label>Profesor</label>
						</div>
          </div>
        </form>
        <button id="send" class="btn waves-effect waves-light">Registrar
          <i class="material-icons right">send</i>
        </button>
      </div>
    </div>
  </div>

  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/materialize.min.js"></script>
  <script type="text/javascript" src="js/picker.time.js"></script>
  <script type="text/javascript">
  $(document).ready(function(){
		$('select').material_select();
    $('.timepicker').pickatime({
      clear: '',
      min: [9,00],
      max: [22,00],
      format: 'HH:i:00',
      container: 'body'
    });
    $('.datepicker').pickadate({
      selectMonths: true, // Creates a dropdown to control month
      selectYears: 5, // Creates a dropdown of 15 years to control year
      monthsFull: ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'Noviembre', 'Diciembre'],
      monthsShort: ['ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dec'],
      weekdaysFull: ['domingo', 'lunes', 'martes', 'miércoles', 'jueves', 'viernes', 'sábado'],
      weekdaysShort: ['dom', 'lun', 'mar', 'mié', 'jue', 'vie', 'sáb'],
      today: '',
      clear: '',
      close: '',
      labelMonthNext: '',
      labelMonthPrev: '',
      labelMonthSelect: '',
      labelYearSelect: '',
      format: 'yyyy-mm-dd',
      container: 'body'
    });
    $('.modal').modal({
      startingTop: '1000%',
      endingTop: '5%',
    });
    $('#send').click(function(){
			var frecuencySum = 0;
      $.each($("input[name='frecuency']:checked"), function(){
          frecuencySum += parseInt($(this).val());
      });
			if($('#name').val() == '' || $('#idUser').val() == null){
				Materialize.toast("Completar los datos", 2000);
			} else{
				$.post(
					'models/SetCourse.php',
					{
						id : "0",
						name : $('#name').val(),
						startDate : $('#startDate').val(),
						endDate : $('#endDate').val(),
						frecuency : frecuencySum,
						hourStart : $('#hourStart').val(),
						hourEnd : $('#hourEnd').val(),
						idUser : $('#idUser').val()
					},
					function(data){
						location.reload();
					}
				);
			}
    });
    $('.delete').click(function(){
      $.get(
        'models/DeleteCourse.php',
        {
          id : $(this).attr('idCourse')
        },
        function(data){
          location.reload();
        }
      );
    });
  });
  </script>
</body>
</html>
