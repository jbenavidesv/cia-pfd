<!DOCTYPE html>
<html>
	<head>
		<!--Import Google Icon Font-->
		<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<!--Import materialize.css-->
		<link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
		<link type="text/css" rel="stylesheet" href="css/jquery.dataTables.min.css"/>
		<link rel="stylesheet" href="css/style.css">
		<!--Let browser know website is optimized for mobile-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	</head>

	<body>
		<?php include "models/Conection.php"; ?>
		<?php include "navbar.php"; ?>
		<div class="container">
			<div class="divider"></div>
			<div class="section">
				<h5>Alumnos</h5>
				<table id="dataTable" class="striped">
					<thead>
						<tr>
							<th>Título</th>
							<th>Nombre</th>
							<th>Correo-e</th>
							<th>Teléfono</th>
							<th>Academia</th>
							<th>Editar</th>
							<th>Eliminar</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$link = Conection::conect();
						$result = $link->query("call getStudents();");
						while ($row = mysqli_fetch_array($result)) {
							?>
							<tr>
								<td><?php echo $row['title'] ?></td>
								<td><?php echo $row['name'] ?></td>
								<td><?php echo $row['email'] ?></td>
								<td><?php echo $row['phone'] ?></td>
								<td><?php echo $row['academy'] ?></td>
								<td><a href="#" class="edit" idUser="<?php echo $row['id'] ?>"><i class="material-icons">mode_edit</i></a></td>
								<td><a href="#" class="delete" idUser="<?php echo $row['id'] ?>"><i class="material-icons">delete</i></a></td>
							</tr>
							<?php
						}
						$link->close();
						?>
					</tbody>
				</table>
			</div>
		</div>

		<div class="fixed-action-btn">
			<a id="fabModal" class="btn-floating btn-large red" href="#modal1">
				<i class="large material-icons">add</i>
			</a>
		</div>

		<div id="modal1" class="modal">
			<div class="modal-content">
				<h5 id="labelStudent">Nuevo Alumno</h5>
				<div class="row">
					<form id="form" class="col s12">
						<input type="hidden" id="idUser" name="id" value="0">
						<div class="row">
							<div class="input-field col s2">
								<i class="material-icons prefix">account_circle</i>
								<input id="title" type="text" class="validate">
								<label for="title">Título</label>
							</div>
							<div class="input-field col s4">
								<input id="name" type="text" class="validate">
								<label for="name">Nombre(s)</label>
							</div>
							<div class="input-field col s3">
								<input id="fatherName" type="text" class="validate">
								<label for="fatherName">Apellido Paterno</label>
							</div>
							<div class="input-field col s3">
								<input id="motherName" type="text" class="validate">
								<label for="motherName">Apellido Materno</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s4">
								<i class="material-icons prefix">email</i>
								<input id="email" type="email" class="validate">
								<label data-error="Usa un correo válido" for="email">Correo-e</label>
							</div>
							<div class="input-field col s4">
								<i class="material-icons prefix">phone</i>
								<input id="phone" type="text" class="validate">
								<label for="phone">Teléfono</label>
							</div>
							<div class="input-field col s4">
								<select id="academy">
									<option value="" disabled selected>Elige una academia</option>
										<?php
											$link = Conection::conect();
											$result = $link->query("call getAcademies();");
											while ($row = mysqli_fetch_array($result)) {
										?>
											<option value="<?php echo $row['id'] ?>"><?php echo $row['name'] ?></option>
										<?php }
											$link->close();
										?>
								</select>
								<label>Academia</label>
							</div>
						</div>
					</form>
					<button id="send" class="btn waves-effect waves-light"><span id="labelSend">Registrar</span>
						<i class="material-icons right">send</i>
					</button>
				</div>
				<div id="courses">
				</div>
			</div>
		</div>

		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/materialize.min.js"></script>
		<script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			$('#dataTable').DataTable({
				"paging" :   false,
        "info" :     false,
				"searching" : false,
				"language": {
            "emptyTable": "No hay registros"
        },
				"columnDefs": [
					{ "orderable": false, "targets": 5 },
					{ "orderable": false, "targets": 6 }
				]
			});
			$('.modal').modal({
				complete: function() {
					$('#idUser').val("0");
					$('#title').val("");
					$('#name').val("");
					$('#fatherName').val("");
					$('#motherName').val("");
					$('#email').val("");
					$('#phone').val("");
					$('#courses').html("");
					$('#academy').val(0);
					$('#academy').material_select();
				}
			});
			$('select').material_select();
			$('#fabModal').click(function() {
				$('#labelStudent').html("Nuevo Estudiante");
				$('#labelSend').html("Registrar");
			});
			$('#send').click(function(){
				if(
						$('#nameStudent').val() == '' ||
						$('#academy').val() == null
					){
						Materialize.toast("Completar los datos", 2000);
					} else{
						$.post(
							'models/SetUser.php',
							{
								id : $('#idUser').val(),
								title : $('#title').val(),
								name : $('#name').val(),
								fatherName : $('#fatherName').val(),
								motherName : $('#motherName').val(),
								email : $('#email').val(),
								phone : $('#phone').val(),
								userType : 1,
								academy : $('#academy').val()
							},
							function(data){
								location.reload();
							}
						);
					}
			});
			$('.delete').click(function(){
				$.get(
					'models/DeleteUser.php',
					{
						id : $(this).attr('idUser')
					},
					function(data){
						location.reload();
					}
				);
			});
			$('.edit').click(function(){
				var id = $(this).attr('idUser');
				$.get(
					'models/GetUser.php',
					{
						id : $(this).attr('idUser')
					},
					function(data){
						var json = JSON.parse(data);
						$('#idUser').val(id);
						$('#title').val(json[0].title);
						$('#name').val(json[0].name);
						$('#fatherName').val(json[0].fatherName);
						$('#motherName').val(json[0].motherName);
						$('#email').val(json[0].email);
						$('#phone').val(json[0].phone);
						$('#academy').val(json[0].academy);
						$('#academy').material_select();
						Materialize.updateTextFields();
						$('#labelStudent').html("Actualizar Estudiante");
						$('#labelSend').html("Actualizar");
						$('#modal1').modal('open');
						$.get(
							'models/GetCoursesByStudent.php',
							{
								id : id
							},
							function(data){
								jsona = JSON.parse(data);
								var courses = "<ul class='collection with-header'>"
																+ "<li class='collection-header'><h4>Cursos</h4></li>"
								for (var i = 0; i < jsona.length; i++) {
									courses += "<li class='collection-item'><div>" + jsona[i].name + "<span class='secondary-content'><i class='material-icons'>";
									if (jsona[i].pass == "0"){
										courses += "thumb_down";
									} else {
										courses += "thumb_up";
									}
									courses += "</i></span></div></li>";
								}
								courses += "</ul>";
								$('#courses').append(courses);
							}
						);
					}
				);
			});
		});
		</script>
	</body>
</html>
