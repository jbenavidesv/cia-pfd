<!DOCTYPE html>
<html>
<head>
  <!--Import Google Icon Font-->
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--Import materialize.css-->
  <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
  <link rel="stylesheet" href="css/style.css">
  <!--Let browser know website is optimized for mobile-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body>
  <?php include "models/Conection.php"; ?>
  <?php include "navbar.php"; ?>
  <div class="container">
    <div class="section">
      <div class="row">
        <div class="card">
          <div class="card-content">
            <?php
            $link = Conection::conect();
            $result = $link->query("call getCourse(" . $_GET["id"] . ");");
            $row = mysqli_fetch_array($result)
             ?>
            <span class="card-title"><?php echo $row['name'] ?></span>
              <div class="row">
                <div class="input-field col s6">
                  <input id="name" type="text" class="validate" value="<?php echo $row['name'] ?>">
                  <label for="name">Nombre</label>
                </div>
                <div class="input-field col s3">
                  <input id="startDate" type="date" class="datepicker" value="<?php echo $row['startDate'] ?>">
                  <label for="startDate">Fecha de inicio</label>
                </div>
                <div class="input-field col s3">
                  <input id="endDate" type="date" class="datepicker" value="<?php echo $row['endDate'] ?>">
                  <label for="endDate">Fecha de fin</label>
                </div>
              </div>
              <div class="row">
                <?php if (strpos($row['frecuency'], "lunes") !== false){ ?>
                  <input type="checkbox" class="filled-in" id="1Day" name="frecuency" value="1" checked="true"/>
                <?php } else { ?>
                  <input type="checkbox" class="filled-in" id="1Day" name="frecuency" value="1"/>
                <?php } ?>
                  <label for="1Day">Lunes</label>
                  <?php if (strpos($row['frecuency'], "martes") !== false){ ?>
                    <input type="checkbox" class="filled-in" id="2Day" name="frecuency" value="2" checked="true"/>
                  <?php } else { ?>
                    <input type="checkbox" class="filled-in" id="2Day" name="frecuency" value="2"/>
                  <?php } ?>
                  <label for="2Day">Martes</label>

                  <?php if (strpos($row['frecuency'], "rcoles") !== false){ ?>
                    <input type="checkbox" class="filled-in" id="3Day" name="frecuency" value="4" checked="true"/>
                  <?php } else { ?>
                    <input type="checkbox" class="filled-in" id="3Day" name="frecuency" value="4"/>
                  <?php } ?>
                  <label for="3Day">Miércoles</label>

                  <?php if (strpos($row['frecuency'], "jueves") !== false){ ?>
                    <input type="checkbox" class="filled-in" id="4Day" name="frecuency" value="8" checked="true"/>
                  <?php } else { ?>
                    <input type="checkbox" class="filled-in" id="4Day" name="frecuency" value="8"/>
                  <?php } ?>
                  <label for="4Day">Jueves</label>

                  <?php if (strpos($row['frecuency'], "viernes") !== false){ ?>
                    <input type="checkbox" class="filled-in" id="5Day" name="frecuency" value="16" checked="true"/>
                  <?php } else { ?>
                    <input type="checkbox" class="filled-in" id="5Day" name="frecuency" value="16"/>
                  <?php } ?>
                  <label for="5Day">Viernes</label>

                  <?php if (strpos($row['frecuency'], "bado") !== false){ ?>
                    <input type="checkbox" class="filled-in" id="6Day" name="frecuency" value="32" checked="true"/>
                  <?php } else { ?>
                    <input type="checkbox" class="filled-in" id="6Day" name="frecuency" value="32"/>
                  <?php } ?>
                  <label for="6Day">Sábado</label>
              </div>
              <div class="row">
                <div class="input-field col s3">
                  <input id="hourStart" type="time" class="timepicker" value="<?php echo $row['hourStart'] ?>">
                  <label for="hourStart">Hora de inicio</label>
                </div>
                <div class="input-field col s3">
                  <input id="hourEnd" type="time" class="timepicker" value="<?php echo $row['hourEnd'] ?>">
                  <label for="hourEnd">Hora de fin</label>
                </div>
                <div class="input-field col s6">
							<select id="idUser">
								<option value="" disabled selected>Elige un profesor</option>
								<?php
									$idProfessor = $row['professorName'];
									$link = Conection::conect();
									$result = $link->query("call getProfessors();");
									while ($row = mysqli_fetch_array($result)) {
								?>
									<option value="<?php echo $row['id'] ?>"><?php echo $row['name'] ?></option>
								<?php 
									}
									$link->close();
								?>
							</select>
							<label>Profesor</label>
						</div>
              </div>
          </div> <!-- card-content -->
          <div class="card-action center-align">
            <button id="send" class="btn waves-effect waves-light">Actualizar
              <i class="material-icons right">send</i>
            </button>
          </div>
        </div>

      </div> <!-- End Form -->

      <h5>Alumnos</h5>
      <table class="striped">
        <tr>
          <th>Nombre</th>
          <th>Asistencia</th>
          <th>Eliminar</th>
          <th>Acreditado</th>
        </tr>
        <?php
          $link = Conection::conect();
          $result = $link->query("call getCourseStudents(" . $_GET["id"] . ");");
          while ($row = mysqli_fetch_array($result)) {
        ?>
          <tr>
            <td><?php echo $row['name'] ?></td>
            <td><a href="#" data-activates="slide-out" class="button-collapse" idUser="<?php echo $row['id'] ?>"><i class="material-icons">playlist_add</i></a></td>
            <td><a href="#" class="delete" idUser="<?php echo $row['id'] ?>"><i class="material-icons">delete</i></a></td>
            <td>
              <a href="#" class="pass" idUser="<?php echo $row['id'] ?>" pass="<?php echo $row['pass'] ?>">
                <i class="material-icons">
                  <?php
                    if ($row['pass'] == 0) {
                      echo 'thumb_down';
                    } else {
                      echo 'thumb_up';
                    }
                  ?>
                </i>
              </a>
            </td>
          </tr>
        <?php
          }
          $link->close();
        ?>
      </table>
    </div>
  </div>

  <div class="fixed-action-btn">
    <a class="btn-floating btn-large red" href="#modal1">
      <i class="large material-icons">add</i>
    </a>
  </div>

  <ul id="slide-out" class="side-nav">
    <li>
      <div class="userView">
        <div class="background">
          <h4>Alberto Benavides</h4>
        </div>
        <br>
      </div>
    </li>
    <li>
      <div class="row">
        <div class="input-field col s10">
          <input id="assistanceDate" type="date" class="datepicker">
          <label for="assistanceDate">Fecha de asistencia</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s6">
          <input id="inHour" type="time" class="timepicker">
          <label for="inHour">Hora de entrada</label>
        </div>
        <div class="input-field col s6">
          <input id="outHour" type="time" class="timepicker">
          <label for="outHour">Hora de salida</label>
        </div>
      </div>
    </li>
    <li><a id="setAssistance" href="#!">Registrar</a></li>
    <li><div class="divider"></div></li>
    <li class='assistances'>
      <a class="dropdown-button" href="#!" data-activates="assistance-1">
        <i class="material-icons">call_received</i>23/02/2017 - 12:00
      </a>
    </li>
    <ul id='assistance-1' class='dropdown-content assistances'>
      <li><a href="#!">Eliminar</a></li>
    </ul>
  </ul>

  <div id="modal1" class="modal">
    <div class="modal-content">
      <h5>Nuevo Alumno</h5>
      <div class="row">
        <form id="form" class="col s12">
          <div class="row">
            <div class="input-field col s6">
							<select id="students">
								<option value="" disabled selected>Elige un alumno</option>
									<?php
										$link = Conection::conect();
										$result = $link->query("call getAvailableStudents(" . $_GET["id"] . ");");
										while ($row = mysqli_fetch_array($result)) {
									?>
										<option value="<?php echo $row['id'] ?>"><?php echo $row['name'] ?></option>
									<?php }
										$link->close();
									?>
							</select>
							<label>Alumno</label>
						</div>
          </div>
          <div class="row">
            <div class="input-field col s2">
              <i class="material-icons prefix">account_circle</i>
              <input id="title" type="text" class="validate">
              <label for="title">Título</label>
            </div>
            <div class="input-field col s4">
              <input id="nameStudent" type="text" class="validate">
              <label for="nameStudent">Nombre(s)*</label>
            </div>
            <div class="input-field col s3">
              <input id="fatherName" type="text" class="validate">
              <label for="fatherName">Apellido Paterno</label>
            </div>
            <div class="input-field col s3">
              <input id="motherName" type="text" class="validate">
              <label for="motherName">Apellido Materno</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s4">
              <i class="material-icons prefix">email</i>
              <input id="email" type="email" class="validate">
              <label data-error="Usa un correo válido" for="email">Correo-e</label>
            </div>
            <div class="input-field col s4">
              <i class="material-icons prefix">phone</i>
              <input id="phone" type="text" class="validate">
              <label for="phone">Teléfono</label>
            </div>
						<div class="input-field col s4">
							<select id="academy">
								<option value="" disabled selected>Elige una academia</option>
									<?php
										$link = Conection::conect();
										$result = $link->query("call getAcademies();");
										while ($row = mysqli_fetch_array($result)) {
									?>
										<option value="<?php echo $row['id'] ?>"><?php echo $row['name'] ?></option>
									<?php }
										$link->close();
									?>
							</select>
							<label>Academia*</label>
						</div>
          </div>
        </form>
        <button id="setStudent" idUser="" class="btn waves-effect waves-light">Registrar
          <i class="material-icons right">send</i>
        </button>
      </div>
    </div>
  </div>

  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/materialize.min.js"></script>
  <script type="text/javascript" src="js/picker.time.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
			$('#idUser').val(<?php echo $idProfessor; ?>);
			$('#idUser').material_select();
      $(".button-collapse").sideNav({
        edge : 'right'
      });
      // Initialize collapsible (uncomment the line below if you use the dropdown variation)
      $('.collapsible').collapsible();
      $('.timepicker').pickatime({
        clear: '',
        min: [9,00],
        max: [22,00],
        format: 'HH:i:00',
        container: 'body'
      });
			$('select').material_select();
      $('#students').autocomplete({
        data: {
          <?php
          $link = Conection::conect();
          $result = $link->query("call getAvailableStudents(" . $_GET["id"] . ");");
          while ($row = mysqli_fetch_array($result)) {
            ?>
            "<?php echo $row['name'] ?>" : null,
          <?php }
          $link->close()
          ?>
        }
      });
      $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 5, // Creates a dropdown of 15 years to control year
        monthsFull: ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'Noviembre', 'Diciembre'],
        monthsShort: ['ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dec'],
        weekdaysFull: ['domingo', 'lunes', 'martes', 'miércoles', 'jueves', 'viernes', 'sábado'],
        weekdaysShort: ['dom', 'lun', 'mar', 'mié', 'jue', 'vie', 'sáb'],
        today: '',
        clear: '',
        close: '',
        labelMonthNext: '',
        labelMonthPrev: '',
        labelMonthSelect: '',
        labelYearSelect: '',
        format: 'yyyy-mm-dd',
        container: 'body'
      });
      $('.modal').modal({
        startingTop: '1000%',
        endingTop: '5%',
      });
      $('#setStudent').click(function(){
        if($('#students').val() == null){
					if(
						$('#nameStudent').val() == '' ||
						$('#academy').val() == null
					){
						Materialize.toast("Completar los datos", 2000);
					} else{
						$.post(
							'models/SetStudent.php',
							{
								newStudent : 1,
								title : $('#title').val(),
								name : $('#nameStudent').val(),
								fatherName : $('#fatherName').val(),
								motherName : $('#motherName').val(),
								email : $('#email').val(),
								phone : $('#phone').val(),
								userType : 1,
								idCourse : <?php echo $_GET["id"] ?>,
								academy : $('#academy').val()
							},
							function(data){
								location.reload();
							}
						);
					}
        } else {
					if($('#students').val() == null){
						Materialize.toast("Completar los datos", 2000);
					} else{
						$.post(
            'models/SetStudent.php',
            {
              newStudent : 0,
              idUser : $('#students').val(),
              idCourse : <?php echo $_GET["id"] ?>
            },
            function(data){
              location.reload();
            }
          );
					}
        }
      });
      $('.delete').click(function(){
        $.get(
          'models/DeleteCourseStudent.php',
          {
            idUser : $(this).attr('idUser'),
            idCourse : <?php echo $_GET["id"] ?>
          },
          function(data){
            location.reload();
          }
        );
      });
      $('.button-collapse').click(function(){
        $("#setAssistance").attr('idUser', $(this).attr('idUser'));
        $.post(
          'models/GetAssistance.php',
          {
            idUser : $(this).attr('idUser'),
            idCourse : <?php echo $_GET["id"] ?>
          },
          function(data){
            $(".assistances").remove();
            $('#slide-out').append(data);
            $('.dropdown-button').dropdown();
            $('.deleteAssistance').click(function(){
              $.post(
                'models/DeleteAssistance.php',
                {
                  id : $(this).attr('idCourse')
                },
                function(data){
                  location.reload();
                }
              );
            });
          }
        );
      });
      $('#setAssistance').click(function(){
				if(
					$("#assistanceDate").val() != '' &&
					(
						$("#inHour").val() != '' ||
						$("#outHour").val() != ''
					)
				){
					$.post(
          'models/SetAssistance.php',
						{
							idUser : $(this).attr('idUser'),
							idCourse : <?php echo $_GET["id"] ?>,
							assistanceDate : $("#assistanceDate").val(),
							inHour : $("#inHour").val(),
							outHour : $("#outHour").val()
						},
						function(data){
							location.reload();
						}
					);
				} else {
					Materialize.toast("Completar los datos", 2000);
				}
      });
      $('#send').click(function(){
        var frecuencySum = 0;
        $.each($("input[name='frecuency']:checked"), function(){
            frecuencySum += parseInt($(this).val());
        });
        $.post(
          'models/SetCourse.php',
          {
            id : <?php echo $_GET["id"] ?>,
            name : $('#name').val(),
            startDate : $('#startDate').val(),
            endDate : $('#endDate').val(),
            frecuency : frecuencySum,
            hourStart : $('#hourStart').val(),
            hourEnd : $('#hourEnd').val(),
            idUser : $('#idUser').val()
          },
          function(data){
            location.reload();
          }
        );
      });
      $('.pass').click(function(){
        $.get(
          'models/SetPassCourse.php',
          {
            idUser : $(this).attr('idUser'),
            idCourse : <?php echo $_GET["id"] ?>,
            pass : $(this).attr('pass')
          },
          function(data){
            location.reload();
          }
        );
      });
    });
  </script>
</body>
</html>
