<!DOCTYPE html>
<html>
	<head>
		<!--Import Google Icon Font-->
		<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<!--Import materialize.css-->
		<link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
		<link rel="stylesheet" href="css/style.css">
		<!--Let browser know website is optimized for mobile-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	</head>

	<body>
		<?php include "models/Conection.php"; ?>
		<?php include "navbar.php"; ?>
		<div class="container">
			<div class="divider"></div>
			<div class="section">
				<h5>Academias</h5>
				<div class="divider"></div>
				<table class="striped">
					<tr>
						<th>Nombre</th>
						<th>Coordinador</th>
						<th>Editar</th>
					</tr>
					<?php
					$link = Conection::conect();
					$result = $link->query("call getAcademies()");
					while ($row = mysqli_fetch_array($result)) {
						?>
						<tr>
							<td><?php echo $row['name'] ?></td>
							<td><?php echo $row['coordinatorName'] ?></td>
							<td><a href="#" class="edit" idAcademy="<?php echo $row['id'] ?>"><i class="material-icons">mode_edit</i></a></td>
						</tr>
						<?php
					}
					$link->close();
					?>
				</table>
			</div>
		</div>

		<div id="modal1" class="modal">
			<div class="modal-content">
				<h5 id="labelAcademy">Nueva Academia</h5>
				<div class="row">
					<form id="form" class="col s12">
						<input type="hidden" id="idAcademy" name="id" value="0">
						<div class="row">
							<div class="input-field col s6">
								<input id="name" type="text" class="validate">
								<label for="name">Nombre</label>
							</div>
							<div class="input-field col s6">
								<select id="idUser">
									<option value="" disabled selected>Elige un profesor</option>
										<?php
											$link = Conection::conect();
											$result = $link->query("call getStudents();");
											while ($row = mysqli_fetch_array($result)) {
										?>
											<option value="<?php echo $row['id'] ?>"><?php echo $row['name'] ?></option>
										<?php }
											$link->close();
										?>
								</select>
								<label>Profesor</label>
							</div>
            </div>
					</form>
					<button id="send" class="btn waves-effect waves-light"><span id="labelSend">Registrar</span>
						<i class="material-icons right">send</i>
					</button>
				</div>
				<div id="courses">
				</div>
			</div>
		</div>

		<div class="fixed-action-btn">
			<a id="fabModal" class="btn-floating btn-large red" href="#modal1">
				<i class="large material-icons">add</i>
			</a>
		</div>

		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/materialize.min.js"></script>
		<script type="text/javascript">
		$(document).ready(function(){
			$('#idUser').material_select();
			$('.modal').modal({
				complete: function() {
					$('#idAcademy').val("0");
					$('#name').val("");
					$('#idUser').val("");
				}
			});
			$('#fabModal').click(function() {
				$('#labelAcademy').html("Nueva Academia");
				$('#labelSend').html("Registrar");
			});
			$('#send').click(function(){
				if($('#name').val() == ""){
					Materialize.toast("Completar los datos", 2000);
				} else{
					$.post(
					'models/SetAcademy.php',
					{
						id : $('#idAcademy').val(),
						name : $('#name').val(),
						idUser : $('#idUser').val()
					},
					function(data){
						location.reload();
					}
				);
				}
			});
			$('.edit').click(function(){
				var id = $(this).attr('idAcademy');
				$.get(
					'models/GetAcademy.php',
					{
						id : $(this).attr('idAcademy')
					},
					function(data){
						var json = JSON.parse(data);
						$('#idAcademy').val(id);
						$('#name').val(json[0].name);
						$('#idUser').val(json[0].id);
						$('#idUser').material_select();
						Materialize.updateTextFields();
						$('#labelAcademy').html("Actualizar Academia");
						$('#labelSend').html("Actualizar");
						$('#modal1').modal('open');
					}
				);
			});
		});
		</script>
	</body>
</html>
