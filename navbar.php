<nav>
  <div class="nav-wrapper">
    <a href="#" class="brand-logo">PFD</a>
    <ul id="nav-mobile" class="right hide-on-med-and-down">
      <li><a href="professors.php">Profesores</a></li>
      <li><a href="students.php">Alumnos</a></li>
      <li><a href="courses.php">Cursos</a></li>
      <li><a href="academies.php">Academias</a></li>
    </ul>
  </div>
</nav>
