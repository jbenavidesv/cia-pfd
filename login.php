<!DOCTYPE html>
<html>
<head>
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
  <div class="col-xs-4 col-xs-offset-4">
    <form>
      <div class="form-group">
        <label>Nombre de usuario</label>
        <input type="text" class="form-control" id="username" placeholder="Nombre de usuario">
      </div>
      <div class="form-group">
        <label>Contraseña</label>
        <input type="password" class="form-control" id="password" placeholder="Contraseña">
      </div>
      <button type="submit" class="btn btn-default">Entrar</button>
    </form>
  </div>

  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
</body>
</html><!DOCTYPE html>
